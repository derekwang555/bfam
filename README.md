# What is the design
* Used EventBus to simulate price update publish/subscribe process
* Created a separate server thread for the price update process
* Used a ConcurrentHashMap to store the reference price
* Made the EventBasedReferencePriceSource class Singleton so only 1 instance exists
* Server will create a new thread for each client connected via the socket

# How to run/test
1. Start server using the QuoteServer class and specify the port number (E.g. java QuoteServer 6868)
2. Start the client using the QuoteClient class and specify the hostname/IP and port number (E.g. java QuoteClient localhost 6868)
3. Enter the request message from the client console

# What are the assumptions
### Reference Price Update
* Created a dummy list of security prices in the code 
* The price will be changed for each security every 1 minute
* The server will automatically run the above to simulate the price update process
### Price Calculation
* Increase 10% of the reference price if it is a buy
* Decrease 10% of the reference price if it is a sell

# What can be improved
* Improve the data validations so that the server can be more robust 
* Add more unit tests
* Improve the exception handling
* Test more complicated requests sending in parallel and improve the server performance and multi-tread handling