package com.example.marketmaker.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.example.marketmaker.entity.QuoteRequest;
import com.example.marketmaker.exception.QuoteProcessException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ClientMessageParserTest {

    private ClientMessageParser parser;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        parser = new ClientMessageParser();
    }

    @Test
    public void testRequestMessageIsBlank() {
        thrown.expect(QuoteProcessException.class);
        thrown.expectMessage("Request message is invalid: ");
        parser.parse(" ");
    }

    @Test
    public void testRequestMessageIsNull() {
        thrown.expect(QuoteProcessException.class);
        thrown.expectMessage("Request message is null");
        parser.parse(null);
    }

    @Test
    public void testRequestMessageBuySellFlagIsInvalid() {
        thrown.expect(QuoteProcessException.class);
        thrown.expectMessage("Cannot parse BuySell flag. String value is ABC");
        parser.parse("123 ABC 100");
    }

    @Test
    public void testRequestMessageSecurityIdIsInvalid() {
        thrown.expect(QuoteProcessException.class);
        thrown.expectMessage("Cannot parse Security Id. String value is ABC");
        parser.parse("ABC BUY 100");

        thrown.expect(QuoteProcessException.class);
        thrown.expectMessage("Invalid Security Id: 111111");
        parser.parse("111111 BUY 100");
    }

    @Test
    public void testRequestMessageQuantityIsInvalid() {
        thrown.expect(QuoteProcessException.class);
        thrown.expectMessage("Invalid Quantity: 0");
        parser.parse("9999 BUY 0");

        thrown.expectMessage("Invalid Quantity: -10");
        parser.parse("9999 BUY -10");
    }

    @Test
    public void testParsRequestMessageSuccess() {
        QuoteRequest request = parser.parse("9999 BUY 100");
        assertEquals(Integer.valueOf(9999), request.getSecurityId());
        assertTrue(request.getIsBuy());
        assertEquals(Integer.valueOf(100), request.getQuantity());
    }

}