package com.example.marketmaker;

import com.example.marketmaker.exception.QuoteProcessException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class QuoteCalculationEngineImplTest {

    private QuoteCalculationEngine engine;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        engine = new QuoteCalculationEngineImpl();
    }

    @Test
    public void testCalculateWithInvalidReferencePrice() {
        thrown.expect(QuoteProcessException.class);
        thrown.expectMessage("Invalid Reference Price: 0");
        engine.calculateQuotePrice(9999, 0, true, 100);
    }

    @Test
    public void testCalculateReferencePriceSuccess() {
        double price = engine.calculateQuotePrice(9999, 2.5, true, 100);
        assertEquals(2.75, price, 0.01);
    }

}