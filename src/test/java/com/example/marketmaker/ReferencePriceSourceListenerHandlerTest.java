package com.example.marketmaker;

import org.junit.Test;

import static org.junit.Assert.*;

public class ReferencePriceSourceListenerHandlerTest {

    @Test
    public void testReferencePriceChanged() {
        ReferencePriceSource source = EventBasedReferencePriceSource.getInstance();
        source.getPriceMap().put(9999, 9.5);
        assertEquals(9.5, source.get(9999), 0.01);

        ReferencePriceSourceListener listener = new ReferencePriceSourceListenerHandler(source);
        listener.referencePriceChanged(9999, 10.5);
        assertEquals(10.5, source.get(9999), 0.01);
    }

}