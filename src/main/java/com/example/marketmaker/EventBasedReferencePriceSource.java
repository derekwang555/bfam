package com.example.marketmaker;

import com.example.marketmaker.entity.PriceChange;
import com.example.marketmaker.exception.QuoteProcessException;
import com.google.common.eventbus.Subscribe;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class EventBasedReferencePriceSource implements ReferencePriceSource{

    private static EventBasedReferencePriceSource INSTANCE;
    private ConcurrentHashMap<Integer, Double> priceMap;
    private ReferencePriceSourceListener listener;

    private EventBasedReferencePriceSource() {
        this.priceMap = new ConcurrentHashMap<Integer, Double>();
    }

    public static EventBasedReferencePriceSource getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new EventBasedReferencePriceSource();
        }
        return INSTANCE;
    }

    @Subscribe
    public void subscribePriceEvent(String msg) {
        System.out.println("Received price update msg: " + msg);
        if (this.listener != null) {
            try {
                PriceChange priceChange = parseMessage(msg);
                listener.referencePriceChanged(priceChange.getSecurityId(), priceChange.getPrice());
            } catch (Exception e) {
                System.out.println("Failed to process price update: " + e.getMessage());
            }
        }
    }

    // Assume the message format for the price event is "securityId price"
    private PriceChange parseMessage(String msg) throws NumberFormatException {
        String[] array = msg.split(" ");
        PriceChange priceChange = new PriceChange();
        priceChange.setSecurityId(Integer.parseInt(array[0]));
        priceChange.setPrice(Double.parseDouble(array[1]));
        return priceChange;
    }

    public void subscribe(ReferencePriceSourceListener listener) {
        this.listener = listener;
    }

    public double get(int securityId) {
        if (priceMap.containsKey(securityId)) {
            return priceMap.get(securityId);
        } else {
            throw new QuoteProcessException("Unable to get reference price for security: " + securityId);
        }
    }

    public Map<Integer, Double> getPriceMap() {
        return this.priceMap;
    }
}
