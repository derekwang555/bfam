package com.example.marketmaker.server;

import com.example.marketmaker.EventBasedReferencePriceSource;
import com.example.marketmaker.QuoteCalculationEngine;
import com.example.marketmaker.QuoteCalculationEngineImpl;
import com.example.marketmaker.ReferencePriceSource;
import com.example.marketmaker.entity.QuoteRequest;
import com.example.marketmaker.exception.QuoteProcessException;

import java.io.*;
import java.net.*;

public class QuoteServerThread extends Thread {
    private Socket socket;
    private ClientMessageParser clientMessageParser;
    private QuoteCalculationEngine quoteCalculationEngine;

    public QuoteServerThread(Socket socket) {
        this.socket = socket;
        clientMessageParser = new ClientMessageParser();
        quoteCalculationEngine = new QuoteCalculationEngineImpl();
    }

    public void run() {
        try {
            InputStream input = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));

            OutputStream output = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(output, true);

            String text;

            do {
                text = reader.readLine();
                System.out.println("Quote Server: received client message - " + text);

                try {
                    QuoteRequest request = clientMessageParser.parse(text);
                    ReferencePriceSource referencePriceSource = EventBasedReferencePriceSource.getInstance();
                    double refPrice = referencePriceSource.get(request.getSecurityId());
                    double quotePrice = quoteCalculationEngine.calculateQuotePrice(request.getSecurityId(), refPrice, request.getIsBuy(), request.getQuantity());

                    writer.println(Math.round(quotePrice * 100.0) / 100.0);
                } catch (QuoteProcessException qpe) {
                    String message = "Unable to process request: " + qpe.getMessage();
                    System.out.println(message);
                    writer.println(message);
                } catch (Exception e) {
                    String message = "Unable to process request: " + e.getMessage();
                    System.out.println(message);
                    writer.println(message);
                }

            } while (!text.equals("exit"));

            socket.close();
            System.out.println("Socket closed for thread - " + Thread.currentThread().getId() + "|" + Thread.currentThread().getName());
        } catch (IOException ex) {
            System.out.println("Server exception: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}