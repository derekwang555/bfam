package com.example.marketmaker.server;

import java.io.*;
import java.net.*;

public class QuoteServer {

    public static void main(String[] args) {
        if (args.length < 1) return;

        new PriceSourceThread().start();

        int port = Integer.parseInt(args[0]);

        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Server is listening on port " + port);

            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("New client connected");

                new QuoteServerThread(socket).start();
            }

        } catch (IOException ex) {
            System.out.println("Server exception: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}

