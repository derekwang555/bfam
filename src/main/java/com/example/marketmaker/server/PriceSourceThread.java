package com.example.marketmaker.server;

import com.example.marketmaker.*;

import java.text.DecimalFormat;
import java.util.Random;

public class PriceSourceThread extends Thread {

    public void run() {
        ReferencePriceSource priceSource = EventBasedReferencePriceSource.getInstance();
        ReferencePriceSourceListener listener = new ReferencePriceSourceListenerHandler(priceSource);
        //priceSource.subscribe(listener);

        EventBusCenter.register(priceSource);

        System.out.println("============   Reference Price Source registered  ====================");
        postPriceUpdates();
    }

    // Send random price update periodically
    private void postPriceUpdates() {
        String secId1 = "700";
        String secId2 = "9988";
        String secId3 = "5";

        double priceSec1 = 600.0;
        double priceSec2 = 220.0;
        double priceSec3 = 50.0;

        while (true) {
            // Generate a random price change percentage number for update
            Random r = new Random();
            double priceChangePercentage = 0.9 + r.nextDouble() * (1.1 - 0.9);
            String updatedPriceSec1 = round(priceSec1 * priceChangePercentage);
            String updatedPriceSec2 = round(priceSec2 * priceChangePercentage);
            String updatedPriceSec3 = round(priceSec3 * priceChangePercentage);
            EventBusCenter.post(secId1 + " " + updatedPriceSec1);
            EventBusCenter.post(secId2 + " " + updatedPriceSec2);
            EventBusCenter.post(secId3 + " " + updatedPriceSec3);
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private String round(double value) {
        DecimalFormat df=new DecimalFormat("0.00");
        return df.format(value);
    }
}
