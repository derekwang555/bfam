package com.example.marketmaker.server;

import com.example.marketmaker.entity.QuoteRequest;
import com.example.marketmaker.exception.QuoteProcessException;

public class ClientMessageParser {

    public QuoteRequest parse(String requestMsg) {
        if (requestMsg != null) {
            String[] split = requestMsg.split(" ");
            if (split.length != 3) {
                throw new QuoteProcessException("Request message is invalid: " + requestMsg);
            }
            String secIdString = split[0];
            String buyOrSellString = split[1];
            String quantityString = split[2];

            Integer secId = getIntValue(secIdString, "Security Id");
            boolean isBuy = parseBuyOrSell(buyOrSellString);
            Integer quantity = getIntValue(quantityString, "Quantity");

            validateSecId(secId);
            validateQuantity(quantity);

            return new QuoteRequest(secId, isBuy, quantity);
        } else {
            throw new QuoteProcessException("Request message is null");
        }
    }

    private boolean parseBuyOrSell(String strValue) {
        if ("BUY".equals(strValue)) {
            return true;
        } else if ("SELL".equals(strValue)) {
            return false;
        } else {
            throw new QuoteProcessException("Cannot parse BuySell flag. String value is " + strValue);
        }
    }

    private Integer getIntValue(String strValue, String name) {
        Integer intValue;
        try {
            intValue = Integer.parseInt(strValue);
        } catch (NumberFormatException exception) {
            throw new QuoteProcessException("Cannot parse " + name + ". String value is " + strValue);
        }
        return intValue;
    }

    private void validateSecId(int secId) {
        if (secId > 99999) {
            throw new QuoteProcessException("Invalid Security Id: " + secId);
        }
    }

    private void validateQuantity(int quantity) {
        if (quantity < 1) {
            throw new QuoteProcessException("Invalid Quantity: " + quantity);
        }
    }
}
