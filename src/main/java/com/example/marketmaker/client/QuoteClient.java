package com.example.marketmaker.client;

import java.net.*;
import java.io.*;
import java.util.Scanner;
import java.util.regex.Pattern;

public class QuoteClient {

    public static void main(String[] args) {
        if (args.length < 2) return;

        String hostname = args[0];
        int port = Integer.parseInt(args[1]);

        try {
            Socket socket = new Socket(hostname, port);
            OutputStream output = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(output, true);

            Scanner sn = new Scanner(System.in).useDelimiter(Pattern.compile("[\\r\\n;]+"));

            // store the user input
            String inputText;

            // loop until exit command is entered
            do {
                System.out.println("Enter command:");
                inputText = sn.next();
                writer.println(inputText);
                InputStream input = socket.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                String response = reader.readLine();
                System.out.println(response);
            } while (!inputText.equals("exit"));
            socket.close();

        } catch (UnknownHostException ex) {

            System.out.println("Server not found: " + ex.getMessage());

        } catch (IOException ex) {

            System.out.println("I/O error: " + ex.getMessage());
        }
    }
}
