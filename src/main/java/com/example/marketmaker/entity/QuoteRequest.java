package com.example.marketmaker.entity;

public class QuoteRequest {
    private Integer securityId;
    private Boolean isBuy;
    private Integer quantity;

    public QuoteRequest(Integer securityId, Boolean isBuy, Integer quantity) {
        this.securityId = securityId;
        this.isBuy = isBuy;
        this.quantity = quantity;
    }

    public Integer getSecurityId() {
        return securityId;
    }

    public void setSecurityId(Integer securityId) {
        this.securityId = securityId;
    }

    public Boolean getIsBuy() {
        return isBuy;
    }

    public void setIsBuy(Boolean isBuy) {
        this.isBuy = isBuy;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
