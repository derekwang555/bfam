package com.example.marketmaker.exception;

public class QuoteProcessException extends RuntimeException {

    public QuoteProcessException(String message) {
        super(message);
    }
}
