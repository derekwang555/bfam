package com.example.marketmaker;

import com.example.marketmaker.exception.QuoteProcessException;

public class QuoteCalculationEngineImpl implements QuoteCalculationEngine {

    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        validateReferencePrice(referencePrice);
        if (buy) {
            return referencePrice * 1.1;
        } else {
            return referencePrice * 0.9;
        }
    }

    private void validateReferencePrice(double price) {
        if (price < 0.0001) {
            throw new QuoteProcessException("Invalid Reference Price: " + price);
        }
    }

}
