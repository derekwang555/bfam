package com.example.marketmaker;

public class ReferencePriceSourceListenerHandler implements ReferencePriceSourceListener {

    private ReferencePriceSource referencePriceSource;

    public ReferencePriceSourceListenerHandler(ReferencePriceSource referencePriceSource) {
        this.referencePriceSource = referencePriceSource;
        // Register the listener to the ReferencePriceSource
        // So ReferencePriceSource can inform the listener when price changes without directly referencing ReferencePriceSourceListenerHandler
        referencePriceSource.subscribe(this);
    }

    public void referencePriceChanged(int securityId, double price) {
        referencePriceSource.getPriceMap().put(securityId, price);
        System.out.println("Price updated: " + securityId + "|" + price);
    }
}
